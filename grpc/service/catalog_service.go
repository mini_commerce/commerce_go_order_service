package service

import (
	"context"

	// "github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"mini_commerce/commerce_go_order_service/config"
	"mini_commerce/commerce_go_order_service/genproto/catalog_service"
	"mini_commerce/commerce_go_order_service/grpc/client"
	"mini_commerce/commerce_go_order_service/pkg/logger"
	"mini_commerce/commerce_go_order_service/storage"
)

type CatalogService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*catalog_service.UnimplementedCatalogServiceServer
}

func NewCatalogService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *CatalogService {
	return &CatalogService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *CatalogService) CreateCatalog(ctx context.Context, req *catalog_service.CraeteCatalogRequest) (resp *catalog_service.Catalog, err error) {

	i.log.Info("---CreateCatalogService------>", logger.Any("req", req))

	rep, err := i.strg.Catalog().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!Catalog->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	resp = rep
	return
}

func (i *CatalogService) GetList(ctx context.Context, req *catalog_service.GetListRequest) (resp *catalog_service.GetCatalogListResponse, err error) {

	i.log.Info("---CatalogService------>", logger.Any("req", req))

	resp, err = i.strg.Catalog().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!Catalog->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CatalogService) GetId(ctx context.Context, req *catalog_service.CatalogPrimaryKey) (resp *catalog_service.CatalogWithProduct, err error) {

	i.log.Info("---CatalogByID------>", logger.Any("req", req))

	resp, err = i.strg.Catalog().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!Catalog->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

// func (i *HobbieService) Update(ctx context.Context, req *user_service.UpdateHobbie) (resp *user_service.GetIdHobbiUser, err error) {

// 	i.log.Info("---UpdateHobbi------>", logger.Any("req", req))

// 	rowsAffected, err := i.strg.Hobbi().Update(ctx, req)

// 	if err != nil {
// 		i.log.Error("!!!UpdateHobbi--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	if rowsAffected <= 0 {
// 		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
// 	}

// 	resp, err = i.strg.Hobbi().GetByHobbie(ctx, &user_service.HobbiePrimaryKey{Id: req.Id})
// 	if err != nil {
// 		i.log.Error("!!!GetUser->User->Get--->", logger.Error(err))
// 		return nil, status.Error(codes.NotFound, err.Error())
// 	}

// 	return resp, err
// }

// func (i *HobbieService) UpdatePatch(ctx context.Context, req *user_service.UpdatePatchHobbie) (resp *user_service.Hobbie, err error) {

// 	i.log.Info("---UpdatePatchEnrolledStudent------>", logger.Any("req", req))

// 	updatePatchModel := models.UpdatePatchRequest{
// 		Id:     req.GetId(),
// 		Fields: req.GetFields().AsMap(),
// 	}

// 	rowsAffected, err := i.strg.Hobbi().UpdatePatch(ctx, &updatePatchModel)

// 	if err != nil {
// 		i.log.Error("!!!UpdatePatchHobbi--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	if rowsAffected <= 0 {
// 		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
// 	}

// 	resp, err = i.strg.Hobbi().GetByPKey(ctx, &user_service.HobbiePrimaryKey{Id: req.Id})
// 	if err != nil {
// 		i.log.Error("!!!GetHobbi->Hobbi->Get--->", logger.Error(err))
// 		return nil, status.Error(codes.NotFound, err.Error())
// 	}

// 	return resp, err
// }

// func (i *HobbieService) Delete(ctx context.Context, req *user_service.HobbiePrimaryKey) (resp *empty.Empty, err error) {

// 	i.log.Info("---DeleteUser------>", logger.Any("req", req))

// 	err = i.strg.Hobbi().Delete(ctx, req)
// 	if err != nil {
// 		i.log.Error("!!!DeleteUser->User->Get--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	return &empty.Empty{}, nil
// }
