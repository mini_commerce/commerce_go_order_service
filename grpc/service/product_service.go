package service

import (
	"context"
	"mini_commerce/commerce_go_order_service/config"
	"mini_commerce/commerce_go_order_service/genproto/catalog_service"
	"mini_commerce/commerce_go_order_service/grpc/client"
	"mini_commerce/commerce_go_order_service/pkg/logger"
	"mini_commerce/commerce_go_order_service/storage"

	// "github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*catalog_service.UnimplementedProductServiceServer
}

func NewProducService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ProductService {
	return &ProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ProductService) CreateProduct(ctx context.Context, req *catalog_service.CraeteProductRequest) (resp *catalog_service.Product, err error) {

	i.log.Info("---CreateProduct------>", logger.Any("req", req))

	pKey, err := i.strg.Product().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateProduct->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp = pKey
	return
}

func (i *ProductService) GetId(ctx context.Context, req *catalog_service.ProductPrimaryKey) (*catalog_service.Product, error) {

	i.log.Info("---GetProductByID------>", logger.Any("req", req))

	resp, err := i.strg.Product().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetByID->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}


func (i *ProductService) GetList(ctx context.Context, req *catalog_service.GetListRequest) (resp *catalog_service.GetProductListResponse, err error) {

	i.log.Info("---GetProduct------>", logger.Any("req", req))

	resp, err = i.strg.Product().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ProductService) Update(ctx context.Context, req *catalog_service.UpdeteProduct) (resp *catalog_service.Product, err error) {

	i.log.Info("---UpdateProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Product().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Product().GetByPKey(ctx, &catalog_service.ProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}


func (i *ProductService) Delete(ctx context.Context, req *catalog_service.ProductPrimaryKey) (resp *catalog_service.Empty, err error) {

	i.log.Info("---DeleteProduct------>", logger.Any("req", req))

	err = i.strg.Product().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &catalog_service.Empty{}, nil
}