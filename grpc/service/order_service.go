package service

import (
	"context"

	"mini_commerce/commerce_go_order_service/config"
	"mini_commerce/commerce_go_order_service/genproto/catalog_service"
	"mini_commerce/commerce_go_order_service/grpc/client"
	"mini_commerce/commerce_go_order_service/pkg/logger"
	"mini_commerce/commerce_go_order_service/storage"

	// "github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type OrderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*catalog_service.UnimplementedOrderServiceServer
}

func NewOrderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *OrderService {
	return &OrderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *OrderService) CreateOrder(ctx context.Context, req *catalog_service.CraeteOrderRequest) (resp *catalog_service.Order, err error) {

	i.log.Info("---CreateOrder------>", logger.Any("req", req))

	pKey, err := i.strg.Order().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateOrder->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	resp, err = i.strg.Order().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyOrder-Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) GetId(ctx context.Context, req *catalog_service.OrderPrimaryKey) (*catalog_service.Order, error) {

	i.log.Info("---GetOrderByID------>", logger.Any("req", req))

	resp, err := i.strg.Order().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetByID->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}


func (i *OrderService) GetList(ctx context.Context, req *catalog_service.GetListRequest) (resp *catalog_service.GetListOrderResponse, err error) {

	i.log.Info("---GetOrder------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOrder->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}

func (i *OrderService) Update(ctx context.Context, req *catalog_service.UpdeteOrder) (resp *catalog_service.Order, err error) {

	i.log.Info("---UpdateOrder------>", logger.Any("req", req))

	resp, err = i.strg.Order().Update(ctx, req)
	if err != nil {
		i.log.Error("!!!UpdateOrder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, err
}

func (i *OrderService) UpdateStatus(ctx context.Context, req *catalog_service.UpdateOrderStatusRequest) (resp *catalog_service.UpdateOrderStatusResponse, err error) {

	i.log.Info("---UpdateStatus------>", logger.Any("req", req))

	id, err := i.strg.Order().UpdateStatus(ctx, req)
	if err != nil {
		i.log.Error("!!!UpdateStatus--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	
	resp=&catalog_service.UpdateOrderStatusResponse{
		Status: id,
	}
	return resp, err
}

func (i *OrderService) GetOrdersByUserId(ctx context.Context, req *catalog_service.UserPrimaryKey) (resp *catalog_service.GetOrdersByUserIdResponse,err error) {

	i.log.Info("---GetOrdersByUserId------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetOrderByUserId(ctx, req)
	if err != nil {
		i.log.Error("!!!GetByID->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return 
}