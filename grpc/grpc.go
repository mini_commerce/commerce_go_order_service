package grpc

import (
	"mini_commerce/commerce_go_order_service/config"
	"mini_commerce/commerce_go_order_service/genproto/catalog_service"
	"mini_commerce/commerce_go_order_service/grpc/client"
	"mini_commerce/commerce_go_order_service/grpc/service"
	"mini_commerce/commerce_go_order_service/pkg/logger"
	"mini_commerce/commerce_go_order_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	catalog_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))
	catalog_service.RegisterCatalogServiceServer(grpcServer,service.NewCatalogService(cfg, log, strg, srvc))
	catalog_service.RegisterOrderServiceServer(grpcServer,service.NewOrderService(cfg, log, strg, srvc))
	catalog_service.RegisterProductServiceServer(grpcServer, service.NewProducService(cfg, log, strg, srvc))
	// catalog_service.RegisterOrderServiceServer(grpcServer, service.NewOrderService(cfg, log, strg, srvc))
	reflection.Register(grpcServer)
	return
}
