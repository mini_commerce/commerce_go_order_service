package storage

import (
	"context"

	"mini_commerce/commerce_go_order_service/genproto/catalog_service"
	// "mini_commerce/commerce_go_order_service/models"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
	Catalog() CatalogRepoI
	Order() OrderRepoI
	Product() ProductRepoI
}

type UserRepoI interface {
	Create(ctx context.Context, req *catalog_service.CraeteUserRequest) (resp *catalog_service.UserPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.UserPrimaryKey) (resp *catalog_service.User, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListRequest) (resp *catalog_service.GetListResponse, err error)
}

type CatalogRepoI interface {
	Create(ctx context.Context, req *catalog_service.CraeteCatalogRequest) (resp *catalog_service.Catalog, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListRequest) (resp *catalog_service.GetCatalogListResponse, err error)
	GetByPKey(ctx context.Context, req *catalog_service.CatalogPrimaryKey) (resp *catalog_service.CatalogWithProduct, err error)
}

type OrderRepoI interface {
	Create(ctx context.Context, req *catalog_service.CraeteOrderRequest) (resp *catalog_service.OrderPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.OrderPrimaryKey) (resp *catalog_service.Order, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListRequest) (resp *catalog_service.GetListOrderResponse, err error)
	Update(ctx context.Context, req *catalog_service.UpdeteOrder) (resp *catalog_service.Order, err error)
	UpdateStatus(ctx context.Context, req *catalog_service.UpdateOrderStatusRequest) (status string, err error)
	GetOrderByUserId(ctx context.Context, req *catalog_service.UserPrimaryKey) (resp *catalog_service.GetOrdersByUserIdResponse, err error)
}

type ProductRepoI interface {
	Create(ctx context.Context, req *catalog_service.CraeteProductRequest) (resp *catalog_service.Product, err error)
	GetByPKey(ctx context.Context, req *catalog_service.ProductPrimaryKey) (resp *catalog_service.Product, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListRequest) (resp *catalog_service.GetProductListResponse, err error)
	Update(ctx context.Context, req *catalog_service.UpdeteProduct) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *catalog_service.ProductPrimaryKey) error
}
