package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4/pgxpool"

	"mini_commerce/commerce_go_order_service/genproto/catalog_service"
	// "mini_commerce/commerce_go_order_service/models"
	"mini_commerce/commerce_go_order_service/pkg/helper"
	"mini_commerce/commerce_go_order_service/storage"
)

type CatalogRepo struct {
	db *pgxpool.Pool
}

func NewCatalogRepo(db *pgxpool.Pool) storage.CatalogRepoI {
	return &CatalogRepo{
		db: db,
	}
}

func (c *CatalogRepo) Create(ctx context.Context, req *catalog_service.CraeteCatalogRequest) (resp *catalog_service.Catalog, err error) {

	var id = uuid.New()

	query := `INSERT INTO "category" (
				id,
				name
			) VALUES ($1, $2)
		`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
	)
	if err != nil {
		return nil, err
	}
	resp = &catalog_service.Catalog{
		Id:   id.String(),
		Name: req.Name,
	}

	return resp, nil
}

func (c *CatalogRepo) GetAll(ctx context.Context, req *catalog_service.GetListRequest) (resp *catalog_service.GetCatalogListResponse, err error) {

	resp = &catalog_service.GetCatalogListResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "category"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Catalogs = append(resp.Catalogs, &catalog_service.Catalog{
			Id:   id.String,
			Name: name.String,
			// CreatedAt: createdAt.String,
			// UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *CatalogRepo) GetByPKey(ctx context.Context, req *catalog_service.CatalogPrimaryKey) (resp *catalog_service.CatalogWithProduct, err error) {
	query := `
	SELECT
	  c.id,
	  c.name,
	  c.created_at,
	  c.updated_at,
	  JSONB_AGG(JSON_BUILD_OBJECT(
		'id',         p.id,
		'name',       p.name,
		'description',p.description,
		'price',      p.price,
		'quantity',   p.quantity
	  )) as products
	FROM "category" as c
	LEFT JOIN product as p ON p.category_id = c.id
	WHERE c.id = $1
	GROUP BY c.id
`

var (
  id         sql.NullString
  name       sql.NullString
  createdAt  sql.NullString
  updatedAt  sql.NullString
  productObj pgtype.JSONB
)

err = c.db.QueryRow(ctx, query, req.Id).Scan(
  &id,
  &name,
  &createdAt,
  &updatedAt,
  &productObj,
)

if err != nil {
  return resp, err
}

resp = &catalog_service.CatalogWithProduct{
  Id:        id.String,
  Name:      name.String,
}

productObj.AssignTo(&resp.Products)

	return

}

// func (c *HobbiRepo) Update(ctx context.Context, req *user_service.UpdateHobbie) (rowsAffected int64, err error) {

// 	var (
// 		query  string
// 		params map[string]interface{}
// 	)

// 	query = `
// 			UPDATE
// 			    "hobbies"
// 			SET
// 				name = :name,
// 				updated_at = now()
// 			WHERE
// 				id = :id`
// 	params = map[string]interface{}{
// 		"id":   req.GetId(),
// 		"name": req.GetName(),
// 	}

// 	query, args := helper.ReplaceQueryParams(query, params)

// 	result, err := c.db.Exec(ctx, query, args...)
// 	if err != nil {
// 		return
// 	}

// 	return result.RowsAffected(), nil
// }

// func (c *HobbiRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

// 	var (
// 		set   = " SET "
// 		ind   = 0
// 		query string
// 	)

// 	if len(req.Fields) == 0 {
// 		err = errors.New("no updates provided")
// 		return
// 	}

// 	req.Fields["id"] = req.Id

// 	for key := range req.Fields {
// 		set += fmt.Sprintf(" %s = :%s ", key, key)
// 		if ind != len(req.Fields)-1 {
// 			set += ", "
// 		}
// 		ind++
// 	}

// 	query = `
// 		UPDATE
// 			"hobbies"
// 	` + set + ` , updated_at = now()
// 		WHERE
// 			id = :id
// 	`

// 	query, args := helper.ReplaceQueryParams(query, req.Fields)

// 	result, err := c.db.Exec(ctx, query, args...)
// 	if err != nil {
// 		return
// 	}

// 	return result.RowsAffected(), err
// }

// func (c *HobbiRepo) Delete(ctx context.Context, req *user_service.HobbiePrimaryKey) error {

// 	query := `DELETE FROM "hobbies" WHERE id = $1`

// 	_, err := c.db.Exec(ctx, query, req.Id)

// 	if err != nil {
// 		return err
// 	}

// 	return nil
// }

// func (c *HobbiRepo) GetByHobbie(ctx context.Context, req *user_service.HobbiePrimaryKey) (resp *user_service.GetIdHobbiUser, err error) {

// 	query := `
// 		SELECT
// 			id,
// 			name,
// 			created_at,
// 			updated_at
// 		FROM "hobbies"
// 		WHERE id = $1
// 	`

// 	var (
// 		id        sql.NullString
// 		Name      sql.NullString
// 		createdAt sql.NullString
// 		updatedAt sql.NullString
// 	)

// 	err = c.db.QueryRow(ctx, query, req.Id).Scan(
// 		&id,
// 		&Name,
// 		&createdAt,
// 		&updatedAt,
// 	)

// 	if err != nil {
// 		return resp, err
// 	}

// 	resp = &user_service.GetIdHobbiUser{
// 		Id:        id.String,
// 		Name:      Name.String,
// 		CreatedAt: createdAt.String,
// 		UpdatedAt: updatedAt.String,
// 	}

// 	return
// }
