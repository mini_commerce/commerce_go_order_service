package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"mini_commerce/commerce_go_order_service/genproto/catalog_service"
	// "mini_commerce/commerce_go_order_service/models"
	"mini_commerce/commerce_go_order_service/pkg/helper"
	"mini_commerce/commerce_go_order_service/storage"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) storage.ProductRepoI {
	return &ProductRepo{
		db: db,
	}
}

func (c *ProductRepo) Create(ctx context.Context, req *catalog_service.CraeteProductRequest) (resp *catalog_service.Product, err error) {

	var id = uuid.New()

	query := `INSERT INTO "product" (
				id,
				name,
				description,
				price,
				quantity,
				category_id
			) VALUES ($1, $2, $3, $4, $5, $6)
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.Description,
		req.Price,
		req.Quetity,
		req.CatalogId,
	)
	if err != nil {
		return nil, err
	}

	return &catalog_service.Product{
		Id: id.String(),
		Name: req.Name,
		Description: req.Description,
		Price: req.Price,
		Quetity: req.Quetity,
		CatalogId: req.CatalogId,

	}, nil
}

func (c *ProductRepo) GetByPKey(ctx context.Context, req *catalog_service.ProductPrimaryKey) (resp *catalog_service.Product, err error) {

	query := `
		SELECT
			id,
			name,
			description,
			price,
			quantity,
			category_id
		FROM "product"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		name sql.NullString
		description  sql.NullString
		price  sql.NullFloat64
		quantity  sql.NullFloat64
		category_id sql.NullString
	)
// category_id,
	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&description,
		&price,
		&quantity,
		&category_id,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.Product{
		Id:          id.String,
		Name:        name.String,
		Description: description.String,
		Price:       float32(price.Float64),
		Quetity:     int32(quantity.Float64),
		CatalogId: category_id.String,
	}

	return resp, nil
}

func (c *ProductRepo) GetAll(ctx context.Context, req *catalog_service.GetListRequest) (resp *catalog_service.GetProductListResponse, err error) {

	resp = &catalog_service.GetProductListResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
				id,
				name,
				description,
				price,
				quantity,
				category_id
		FROM "product"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			description  sql.NullString
			price  sql.NullFloat64
			quantity  sql.NullFloat64
			category_id sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&description,
			&price,
			&quantity,
			&category_id,
		)

		if err != nil {
			return resp, err
		}

		resp.Products = append(resp.Products, &catalog_service.Product{
			Id:          id.String,
			Name:        name.String,
			Description: description.String,
			Price:       float32(price.Float64),
			Quetity:     int32(quantity.Float64),
			CatalogId: category_id.String,
		})
	}

	return
}

func (c *ProductRepo) Update(ctx context.Context, req *catalog_service.UpdeteProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
				"product"
			SET
				name = :name,
				description = :description,
				price = :price,
				quantity = :quantity
			WHERE
				id = :id`

	params = map[string]interface{}{
		"id":         req.GetId(),
		"name": req.GetName(),
		"description":  req.GetDescription(),
		"price":       req.GetPrice(),
		"quantity":  req.GetQuetity(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)

	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}


func (c *ProductRepo) Delete(ctx context.Context, req *catalog_service.ProductPrimaryKey) error {

	query := `DELETE FROM "product" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
