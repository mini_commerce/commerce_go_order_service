package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"github.com/google/uuid"
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4/pgxpool"

	"mini_commerce/commerce_go_order_service/genproto/catalog_service"
	"mini_commerce/commerce_go_order_service/pkg/helper"
	"mini_commerce/commerce_go_order_service/storage"
)

type OrderRepo struct {
	db *pgxpool.Pool
}

func NewOrderRepo(db *pgxpool.Pool) storage.OrderRepoI {
	return &OrderRepo{
		db: db,
	}
}

func (c *OrderRepo) Create(ctx context.Context, req *catalog_service.CraeteOrderRequest) (resp *catalog_service.OrderPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "order" (
			id,
			client_id
			) VALUES ($1, $2)
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.ClientId,
	)

	if err != nil {
		return nil, err
	}

	ids := strings.Split(req.ProductIds, ",")

	queryUser := `INSERT INTO "order_product" (
		id,
		order_id,
		product_id
		) VALUES 

	`
	for i := 0; i < len(ids); i++ {
		var UserOrderId = uuid.New()
		queryUser += fmt.Sprintf("('%v','%v','%v'),", UserOrderId.String(), id.String(), ids[i])

		if err != nil {
			return nil, err
		}

	}
	queryUser = queryUser[:len(queryUser)-1]

	_, err = c.db.Exec(ctx, queryUser)

	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return &catalog_service.OrderPrimaryKey{Id: id.String()}, nil
}

func (c *OrderRepo) GetByPKey(ctx context.Context, req *catalog_service.OrderPrimaryKey) (resp *catalog_service.Order, err error) {

	query := `
		SELECT
			o.id,
			c.id,
			sum(p.price),
			JSONB_AGG(JSON_BUILD_OBJECT(
			'id',         p.id,
			'name',       p.name,
			'description',p.description,
			'price',      p.price,
			'quantity',   p.quantity
			)) as products
		FROM "order_product" as op
		LEFT JOIN product as p ON p.id = op.product_id 
		JOIN "order" as o on o.id = op.order_id
		JOIN "client" as c on c.id = o.client_id
		WHERE op.order_id= $1
		GROUP BY o.id, c.id
	`

	var (
		id        sql.NullString
		client_id sql.NullString
		price     sql.NullFloat64
		productsobj pgtype.JSONB
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&client_id,
		&price,
		&productsobj,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.Order{
		Id:       id.String,
		ClientId: client_id.String,
		Price:    float32(price.Float64),
	}
	err = productsobj.AssignTo(&resp.Products)
	if err != nil {
		return resp, err
	}

	return 
}

func (c *OrderRepo) GetAll(ctx context.Context, req *catalog_service.GetListRequest) (resp *catalog_service.GetListOrderResponse, err error) {

	resp = &catalog_service.GetListOrderResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " GROUP BY o.id, c.id"
	)

	query = `
			SELECT
			COUNT(*) OVER(),
				o.id,
				c.id,
				sum(p.price),
				JSONB_AGG(JSON_BUILD_OBJECT(
				'id',         p.id,
				'name',       p.name,
				'description',p.description,
				'price',      p.price,
				'quantity',   p.quantity
				)) as products
			FROM "order_product" as op
			LEFT JOIN product as p ON p.id = op.product_id 
			JOIN "order" as o on o.id = op.order_id
			JOIN "client" as c on c.id = o.client_id
			
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			client_id   sql.NullString
			price       sql.NullFloat64
			status      sql.NullString
			productobj pgtype.JSONB
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&client_id,
			&price,
			&productobj,
		)

		if err != nil {
			return resp, err
		}

		res := []*catalog_service.Product{}
		productobj.AssignTo(&res)

		resp.Orders = append(resp.Orders, &catalog_service.Order{
			Id:       id.String,
			ClientId: client_id.String,
			Products: res,
			Price:    float32(price.Float64),
			Status:   status.String,
		})
	}

	return
}

func (c *OrderRepo) Update(ctx context.Context, req *catalog_service.UpdeteOrder) (resp *catalog_service.Order, err error) {

	var (
		query string
	)

	query = `
			UPDATE
				"order"
			SET
				client_id = $1,
				product_ids = $2,
				price = $3,
				status = $4
			WHERE
			client_id = $1`

	_, err = c.db.Exec(ctx, query,
		req.ClientId,
		req.ProductIds,
		req.Price,
		req.Status,
	)

	if err != nil {
		return
	}

	return
}

func (c *OrderRepo) UpdateStatus(ctx context.Context, req *catalog_service.UpdateOrderStatusRequest) (status string, err error) {

	var (
		query string
	)

	query = `
			UPDATE
			    "order"
			SET
				status = $2
			WHERE
				id = $1`

	_, err = c.db.Exec(ctx, query, req.OrderId, req.Status)
	if err != nil {
		return
	}

	return req.Status, nil
}

func (c *OrderRepo) GetOrderByUserId(ctx context.Context, req *catalog_service.UserPrimaryKey) (resp *catalog_service.GetOrdersByUserIdResponse, err error) {

	query := `
	SELECT
		o.id,
		c.id,
		sum(p.price),
		JSONB_AGG(JSON_BUILD_OBJECT(
		'id',         p.id,
		'name',       p.name,
		'description',p.description,
		'price',      p.price,
		'quantity',   p.quantity
		)) as products
	FROM "order_product" as op
	LEFT JOIN product as p ON p.id = op.product_id 
	JOIN "order" as o on o.id = op.order_id
	JOIN "client" as c on c.id = o.client_id
	WHERE o.client_id = $1
	GROUP BY o.id, c.id
`

	var (
		id        sql.NullString
		client_id sql.NullString
		price     sql.NullFloat64
		productsobj pgtype.JSONB
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&client_id,
		&price,
		&productsobj,
	)

	if err != nil {
		return resp, err
	}
	pruduct := []*catalog_service.Product{}

	err = productsobj.AssignTo(&pruduct)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.GetOrdersByUserIdResponse{
		UserId: client_id.String,
	}

	resp.Orders =append(resp.Orders, &catalog_service.Order{
		Id: id.String,
		ClientId: client_id.String,
		Price: float32(price.Float64),
		Products: pruduct,
	})

	
	


	return
}
