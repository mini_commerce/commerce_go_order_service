CREATE TABLE "order_product" (
  "id" uuid PRIMARY KEY,
  "order_id" uuid,
  "product_id" uuid,
  "updated_at" timestamp,
  "created_at" timestamp,
  "deleted_at" timestamp

);