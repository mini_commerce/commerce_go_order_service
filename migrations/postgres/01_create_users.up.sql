CREATE TABLE "client" (
  "id" uuid PRIMARY KEY,
  "first_name" varchar,
  "last_name" varchar,
  "phone_number" varchar NOT NULL,
  "address" varchar,
  "updated_at" timestamp,
  "created_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "product" (
  "id" uuid PRIMARY KEY,
  "name" varchar,
  "category_id" uuid,
  "description" varchar,
  "price" float,
  "quantity" integer,
  "updated_at" timestamp,
  "created_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "category" (
  "id" uuid PRIMARY KEY,
  "name" varchar,
  "updated_at" timestamp,
  "created_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "order" (
  "id" uuid PRIMARY KEY,
  "client_id" uuid,
  "product_ids" varchar,
  "price" float,
  "status" varchar,
  "updated_at" timestamp,
  "created_at" timestamp,
  "deleted_at" timestamp
);

ALTER TABLE "product" ADD FOREIGN KEY ("category_id") REFERENCES "category" ("id");

ALTER TABLE "order" ADD FOREIGN KEY ("client_id") REFERENCES "client" ("id");
